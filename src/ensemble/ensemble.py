#!/usr/bin/env python
import sys
import os
sys.path.append(os.path.abspath("../sem"))
sys.path.append(os.path.abspath("../textrank"))
import feature_extractor
import sem_classify
import cosine
import naive
import lcs
from operator import itemgetter


def normalize(result_lis):
    less = [ sys.maxint for x in range(0, len(result_lis[0][3])) ]
    more = [ -sys.maxint - 1 for x in range(0, len(result_lis[0][3])) ]

    for result in result_lis:
        count = 0
        for score in result[3]:
            if less[count] > score:
                less[count] = score
            if more[count] < score:
                more[count] = score

            count += 1

    for result in result_lis:
        scores = []
        count = 0
        for score in result[3]:
            if more[count] == less[count]:
                scores.append(0)
            else:
                scores.append((score - less[count]) / (more[count] - less[count]))
            count += 1

        result[3] = scores
        result[1] = reduce(lambda x, y: x + y, result[3]) / len(result[3])


def get_summary(lis, top, model_container):
    ret = []
    for doc in lis:
        try:
            res = []
            idx = 0

            sentences = feature_extractor.sentences(doc[2])

            cos = cosine.Cosine(sentences)
            nai = naive.Naive(sentences)
            longestcommon = lcs.LongestCommonSubSeq(sentences)

            cos.GenerateSummary(len(sentences))
            nai.GenerateSummary(len(sentences))
            longestcommon.GenerateSummary(len(sentences))

            for sentence in sentences:
                #feats = feature_extractor.get_feats(sentence, model_container)

                scores = []
                #scores.append(sem_classify.predict(feats, model_container)[1])
                scores.append(cos.GetScore(sentence))
                scores.append(nai.GetScore(sentence))
                scores.append(longestcommon.GetScore(sentence))

                res.append([ idx, reduce(lambda x, y: x + y, scores) / len(scores), sentence, scores ])
                idx += 1

            normalize(res)

            res.sort(key = itemgetter(1), reverse = True)
            top_res = res[:top]
            top_res.sort(key = itemgetter(0))
            gen_sum = [ t[2] for t in top_res ]
                
            ret.append([ doc[0], doc[1], ' '.join(gen_sum) ])

        except:
            pass

    return ret


def main(args):
    if len(args) != 4 or args[1] == '-h':
        print 'Usage: ensemble.py <data-file> <output-file> <model-file>'
        return
    lis = feature_extractor.read_file(args[1])
    model_container = sem_classify.read_model_file(args[3])
    ret = get_summary(lis, 5, model_container)
    feature_extractor.write_file(args[2], ret)


if __name__ == '__main__':
    main(sys.argv)
