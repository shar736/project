from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import cosine_similarity
from textrank import TextRank
from nltk.corpus import stopwords
import nltk.data
import sys
import codecs

class Cosine(TextRank):
    stop = set(stopwords.words('english'))

    def EdgeScore(self, sentence1, sentence2):
        tfidf_vectorizer = TfidfVectorizer()
        tfidf_matrix = tfidf_vectorizer.fit_transform([ ' '.join(set(sentence1.split()) - self.stop), ' '.join(set(sentence2.split()) - self.stop) ])

        return cosine_similarity(tfidf_matrix[0:1], tfidf_matrix[1:2])[0][0]

def Tokenize(text):
    tokenizer = nltk.data.load('tokenizers/punkt/english.pickle')

    sentences = []

    try:
        sentences = tokenizer.tokenize(text)
    except UnicodeDecodeError:
        print "Unicode Error"
       
    return sentences                    

# For testing
def main(args):

    if args[0]=='-h':
        print "Usage: python cosine.py <document> <number:top N setences>"
        return

    doc = codecs.open(args[0], mode = 'r', encoding = 'utf-8', errors = 'strict', buffering = 1)
    text = doc.read()
    doc.close()

    sentences = Tokenize(text)

    cos = Cosine(sentences)
    rank = cos.GenerateSummary(int(args[1]))
    for sentence in cos.GenerateSummary(int(args[1])):
        print sentence

    print (sentences[0] + ":"+str(cos.GetScore(sentences[0])))
    
   
if __name__=="__main__":
    main(sys.argv[1:])

