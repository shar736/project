from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import cosine_similarity
from nltk.corpus import stopwords
import nltk.data
from textrank import TextRank
import sys


class Naive(TextRank):
    stop = set(stopwords.words('english'))

    def EdgeScore(self, sentence1, sentence2):
        sen1 = set(sentence1.split()) - self.stop
        sen2 = set(sentence2.split()) - self.stop

        return len(sen1 & sen2) / len(sen1 | sen2) 


# For testing
def main(args):

    if args[0]=='-h':
        print "Usage: python naive.py <document> <number:top N setences>"
        return
    nav = Naive()
    for sentence in nav.GenerateSummary(args[0], int(args[1])):
        print sentence[0]

   
if __name__=="__main__":
    main(sys.argv[1:])

