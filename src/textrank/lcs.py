import sys
from nltk.corpus import stopwords
from textrank import TextRank
import nltk.data


class LongestCommonSubSeq(TextRank):
    stop = set(stopwords.words('english'))

    def EdgeScore(self, sentence1, sentence2):
        #dont remove stop words 
        l1 = sentence1.split()
        l2=sentence2.split()
        longest = self.LCS(l1, l2) 
        return longest

    def LCS(self,l1, l2):

        if len(l1)==0 or len(l2)==0:
            return 0

        #if l1[0]==l2[0]:
        #    return 1+self.LCS(l1[1:],l2[1:])

        #return max(self.LCS(l1,l2[1:]),self.LCS(l1[1:],l2))
        m = len(l1)-1
        n = len(l2)-1
       
        memo = dict()
        
        for i in range(len(l1)):
            for j in range(len(l2)):
                if i == 0 or j== 0:
                    self.Memoize(memo, i, j,0)
                elif l1[i-1] == l2[j-1]:
                    self.Memoize(memo,i,j,memo[i-1][j-1]+1) 
                else:
                    self.Memoize(memo, i, j,max(memo[i-1][j],memo[i][j-1])) 

        return memo[m][n]

    def Memoize(self,memo, m, n, v):

        if m not in memo.keys():
            memo[m] =dict()

        memo[m][n] = v

def main(args):

    if args[0]=='-h':
        print "Usage: python lcs.py <document> <number:top N setences>"
        return

    lcs = LongestCommonSubSeq()

    for sentence in lcs.GenerateSummary(args[0],int(args[1])):
        print sentence[0]

if __name__=="__main__":
    main(sys.argv[1:])
