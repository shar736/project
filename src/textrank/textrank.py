import sys
import os
import io
import re
import nltk
import codecs
import operator
import networkx as nx

damping_factor = 0.85
OUTGOINGSUM= "outgoing_sum"
class TextRank:
    
    def __init__(self,sentenceList):
        self.sentences = set(sentenceList)
        self.idxToSent= dict(enumerate(self.sentences))
        self.sentToIdx = {self.idxToSent[k]:k for k in self.idxToSent}
        self.rankList = dict()

    def GenerateSummary(self, n):
        graph = self.GetRankGraph(self.sentences)
        rank = self.GetRankList(graph)
        return rank[:n]

    # Dummy implementation
    def EdgeScore(self, sentence1, sentence2):
        return 1
    
    def GetRankGraph(self, sentences):
        graph = dict()

        for s1 in sentences:
            if s1 not in graph.keys():
                graph[s1] = dict()

            for s2 in sentences:
                if s2 in graph and s1 in graph[s2]:
                    graph[s1][s2] = graph[s2][s1]
                else:
                    graph[s1][s2] = self.EdgeScore(s1, s2)
    
        return graph
    
    # Computes the textrank for each sentence, sorts them in descending order and 
    # returns it.
    def GetRankList(self, graph):
        D=nx.Graph()
        tuplelist =[]
        for u in graph:
            for v in graph[u]:
                tuplelist.append((str(self.sentToIdx[u]),str(self.sentToIdx[v]),graph[u][v]))

        D.add_weighted_edges_from(tuplelist)
        scorelist= nx.pagerank(D, max_iter=200)
        self.rankList = {self.idxToSent[int(k)]:scorelist[k] for k in scorelist.keys()}

        return sorted(self.rankList.items(), key = operator.itemgetter(1), reverse =True)

    # Get score for a specific sentence
    def GetScore(self,sentence):
       return self.rankList[sentence] 

# For testing
def Tokenize(text):
    tokenizer = nltk.data.load('tokenizers/punkt/english.pickle')

    sentences = []

    try:
        sentences = tokenizer.tokenize(text)
    except UnicodeDecodeError:
        print "Unicode Error"
       
    return sentences                    

# For testing
def main(args):

    if args[0] == '-h':
        print "Usage: python textrank.py <document> <number:top N setences>"
        return
    doc = codecs.open(args[0], mode = 'r', encoding = 'utf-8', errors = 'strict', buffering = 1)
    text = doc.read()
    doc.close()

    sentences = Tokenize(text)

    tr = TextRank(sentences)

    print tr.GenerateSummary(int(args[1]))
   
if __name__=="__main__":
    main(sys.argv[1:])

