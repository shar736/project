Rebel leaders Yogendra Yadav and Prashant Bhushan were on Monday expelled from the Aam Aadmi Party. Along with the two founding members, senior leaders Anand Kumar and Ajit Jha have also been expelled by AAP’s national disciplinary action committee which found all four guilty of anti-party activities.

“The party’s national disciplinary committee has taken a decision to expel Prashant Bhusan, Yogendra Yadav, Anand Kumar and Ajit Jha. They were expelled for gross indiscipline, anti-party activities and violation of code of conduct of the party,” said party spokesperson Deepak Bajpai.

Mr. Yadav and Mr. Bhushan had called a meeting of party volunteers last week to launch a Swaraj Abhiyan (self-rule campaign) which would potentially lay the ground for the formation of a breakaway party. Though the two had indicated that they would not quit the party, the volunteers meet was considered extreme provocation by AAP’s leadership which immediately set the process of removing them from the party in motion.

The disciplinary committee had served a showcause notice to both leaders last week, listing a number of allegations including a long standing allegation that the two conspired in a bid to make the party lose the recent Delhi elections. The party, in a press release said, the responses to the show-cause notices by the four rebels were found to be unsatisfactory.

Earlier in the day, both Mr. Bhushan and Mr. Yadav had sent scathing responses to the notices issued to them.

“The kangaroo trials, expulsions, witch-hunts, character assassination, rumour campaigns and emotional theatre to justify such macabre acts — all this is so true of the Stalinist regime,” Mr. Yadav said in his reply.

Mr. Bhushan also hit back against the disciplinary committee, accusing its members Pankaj Gupta of accepting donations from dubious companies and Ashish Khetan of doing a “paid news” story, favouring a company. He also said that the panel, which he had headed until last month, was reconstituted illegally. “I am not aware who has constituted this National Disciplinary Committee,” ” he said.
