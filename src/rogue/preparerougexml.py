#!/usr/bin/env python
import sys
import os

header = "<ROUGE-EVAL version=\"1.0\">"+os.linesep
footer ="</ROUGE-EVAL>"

def main(args):
    if args[0]=='-h':
        print "Usage: python preparerougexml.py <generated html files folder> <reference html files folder> <output>"+os.linesep
        print "Description: Generate an XML configuration file used by ROUGE"
        return 

    gen_sum_ids =[]

    for f in os.listdir(args[0]):
        gen_sum_ids.append(f.split('.')[0])

    gen_sum_ids.sort()

    ref_sum_ids = []
    for f in os.listdir(args[1]):
        ref_sum_ids.append(f.split('.')[0])

    output_xml = args[2]

    ref_sum_ids.sort()
    
    rougeXML = open(output_xml, "w")
    rougeXML.write(header)
    for i in range(len(gen_sum_ids)):
        peer_model = "<EVAL ID=\""+str(i+1)+"\">"+os.linesep+\
                "<PEER-ROOT>"+os.linesep+\
                args[0]+os.linesep+\
                "</PEER-ROOT>"+os.linesep+\
                "<MODEL-ROOT>"+os.linesep+\
                args[1]+os.linesep+\
                "</MODEL-ROOT>"+os.linesep+\
                "<INPUT-FORMAT TYPE=\"SEE\">"+os.linesep+\
                "</INPUT-FORMAT>"+os.linesep+\
                "<PEERS>"+os.linesep+\
                "<P ID=\"1\">"+gen_sum_ids[i] + ".html" + "</P>"+os.linesep+\
                "</PEERS>"+os.linesep+\
                "<MODELS>"+os.linesep+\
                "<M ID=\"A\">"+ref_sum_ids[i]+ ".html" + "</M>"+os.linesep+\
                "</MODELS>"+os.linesep+\
                "</EVAL>"+os.linesep

        rougeXML.write(peer_model)
    rougeXML.write(footer)
    rougeXML.close()

if __name__=="__main__":
    main(sys.argv[1:])
