#!/bin/bash
ROUGE="./ROUGE-1.5.5.pl";
"$ROUGE" -e rouge-data -c 95 -2 -1 -U -r 1000 -n 4 -w 1.2 -a "$1"
