#!/usr/bin/env python
import sys
import os
sys.path.append(os.path.abspath("../sem"))
import feature_extractor
import codecs

ARTICLE_ID = "nytimes_article_"

def preparehtml(article_id,summary):
    msg = "<html>" + os.linesep + \
    "<head>" + os.linesep + \
    "<title>" + article_id + "</title>" + os.linesep + \
    "</head>"+os.linesep + \
    "<body bgcolor=\"white\">" + os.linesep

    count = 1
    for sentence in feature_extractor.sentences(summary):
        msg += "<a name=\"" + str(count) + "\">[" + str(count) + "]</a> <a href=\"#" + str(count) + "\" id=" + \
                str(count) + ">" + sentence + "</a>" + os.linesep
        count += 1
        if count == 4:
            break

    msg += "</body>"

    return msg

def main(args):

    if args[0] == '-h':
        print "Usage: python extract.py <output file path from ensemble> <folder-path>"+os.linesep
        print "Description: Extract the generated summaries and reference summaries from"+os.linesep+\
                "the output file to build the html files under specified folder location."+os.linesep+\
                "These html files are refered in the ROUGE xml config file."
        return
    #input_file = open(args[0], "r")
    lis = feature_extractor.read_file(args[0])
    output_dir = args[1]
    #lines = input_file.readlines()
    count = 0
    length = len(lis)
    for line_parts in lis:

        #line_parts = line.split('\t')

        article_id = ARTICLE_ID+str(count)

        ref_sum_file = codecs.open(output_dir + "/ref/" + "ref_sum_" + article_id + ".html", mode = 'w', encoding = 'utf-8', errors = 'strict', buffering = 1)

        htmlformat = preparehtml("ref_sum_"+article_id, line_parts[1])
        ref_sum_file.write(htmlformat)
        ref_sum_file.close()

        article_file = codecs.open(output_dir + "/gen/" + "gen_sum_" + article_id + ".html", mode = 'w', encoding = 'utf-8', errors = 'strict', buffering = 1)
        htmlformat = preparehtml("gen_sum_" + article_id, line_parts[2])
        article_file.write(htmlformat)
        article_file.close()
        
        count += 1

if __name__=="__main__":
    main(sys.argv[1:])
