#!/usr/bin/env python
import sys
import json
import operator
import codecs
from functools import reduce
from math import log
import feature_extractor


def read_model_file(file_name):
    f = codecs.open(file_name, mode = 'r', encoding = 'utf-8', errors = 'strict', buffering = 1)

    model = {}
    prior = {}
    label_counts = {}

    dicts = json.load(f)
    model = dicts[0]
    prior = dicts[1]
    f.close()

    # Calculate some values
    for label in model:
        label_counts[label] = 0
        for word in model[label]:
            label_counts[label] += model[label][word]

    word_set = set()
    for label in model:
        word_set |= set(model[label].keys())
    k = len(word_set)

    return { 'MODEL' : model, 'PRIOR' : prior, 'COUNTS' : label_counts, 'WORD_COUNT' : k }


def predict(item, model_container):
    model = model_container['MODEL']
    prior = model_container['PRIOR']
    label_counts = model_container['COUNTS']
    k = model_container['WORD_COUNT']

    true_label = item[0]
    bag = item[1:]

    total_files = 0
    for key in prior:
        total_files += prior[key]

    sums = {}
    for label in model:
        sums[label] = 0
        for word in bag:
            if word in model[label]:
                sums[label] += log(model[label][word] + 1) - log(label_counts[label] + k)
            else:
                sums[label] += log(1) - log(label_counts[label] + k)

        sums[label] += log(prior[label]) - log(total_files)

    prediction = max(sums, key = sums.get)
    return [ prediction, sums['POS'] ]


def predict_all(lis, model_container):
    res = []
    for item in lis:
        res.append(predict(item, model_container))
    return res


def main(argv):
    if len(argv) != 3 or argv[1] == '-h':
        print 'Usage: sem_classify.py <model-file> <input-data-file>'
        return

    model_container = read_model_file(argv[1])
    lis = feature_extractor.read_file(argv[2])
    lis = feature_extractor.extract_all(lis, skip_summary = True)
    lis = predict_all(lis, model_container)
    feature_extractor.write_file(sys.stdout.name, lis)


if __name__ == '__main__':
    main(sys.argv)
