#!/usr/bin/env python3

import sys
import json
import codecs
import random
from math import log

k = 0
total_files = 0

def read_file(file_name):
    file = codecs.open(file_name, mode = 'r', encoding = 'utf-8', errors = 'strict', buffering = 1)

    ret = []
    for line in file:
        fields = line.split()
        ret.append('\t'.join(fields))

    file.close()
    return set(ret)


def build_nb_model(label_n_feats, model, prior):
    for line in label_n_feats:
        splits = line.split('\t')
        label = splits[0]
        features = splits[1:]

        if label not in model:
            model[label] = {}
        if label not in prior:
            prior[label] = 0

        label_dict = model[label]
        prior[label] += 1

        for feature in features:
            if feature not in label_dict:
                label_dict[feature] = 0
            label_dict[feature] += 1


def predict(label_n_feats, model, prior):
    global k, total_files

    true_label = label_n_feats[0]
    bag = label_n_feats[1:]

    if k == 0:
        word_set = set()
        for label in model:
            word_set |= model[label].keys()

        k = len(word_set)

    if total_files == 0:
        for key in prior:
            total_files += prior[key]

    label_counts = {}
    for label in model:
        label_counts[label] = 0
        for word in model[label]:
            label_counts[label] += model[label][word]

    sums = {}
    for label in model:
        sums[label] = 0
        for word in bag:
            if word in model[label]:
                try:
                    sums[label] += log(model[label][word] + 1) - log(label_counts[label] + k)
                except:
                    print(str(model[label][word] + 1) + "\t" + str(label_counts[label] + k))
                    exit()
            else:
                sums[label] += log(1) - log(label_counts[label] + k)

        try:
            sums[label] += log(prior[label]) - log(total_files)
        except:
            print(str(label) + '\t' + str(prior[label]) + '\t' + str(total_files))
            exit()

    prediction = max(sums, key = sums.get)
    return { 'PRED' : prediction, 'DICT' : sums }


def i_em(model, prior, positive, mixed, itr):
    build_nb_model(positive | mixed, model, prior)
    for i in range(1, itr + 1):
        print("iteration " + str(i) + " ...")
        shift = 0
        for doc in mixed:
            splits = doc.split('\t')
            label = splits[0]
            feats = splits[1:]

            pred = predict(splits, model, prior)['PRED']
            # new label for this mixed example, change the model
            if pred != label:
                shift += 1
                for feat in feats:
                    if feat not in model[pred]:
                        model[pred][feat] = 0
                    model[pred][feat] += 1

                    if feat in model[label] and model[label][feat] > 0:
                        model[label][feat] -= 1

                prior[pred] += 1
                prior[label] -= 1
        print("shifted: " + str(shift))


def get_spy(pos, s):
    return set(random.sample(pos, (int)(len(pos) * s / 100)))


def neg_label(spy):
    ret = []
    for doc in spy:
        ret.append('\t'.join(['NEG'] + doc.split('\t')[1:]))
    return set(ret)


# s - %age of spy from pos
# l - noise level, %age noise in pos
# i - num iterations
def s_em(model, prior, pos, mix, s, l, i):
    neg = set()
    unl = set()

    spy = get_spy(pos, s)
    pos_spy = pos - spy
    spy = neg_label(spy)
    mix_spy = mix | spy
    print("pos_spy: " + str(len(pos_spy)))
    print("mix_spy: " + str(len(mix_spy)))

    i_em(model, prior, pos_spy, mix_spy, i)

    values = []
    for doc in spy:
        splits = doc.split('\t')
        label = splits[0]
        feats = splits[1:]

        values.append(predict(splits, model, prior)['DICT']['POS'])

    values.sort(reverse = True)
    index = (int)(len(values) * (100 - l) / 100)
    threshold = values[index + 1]

    data = []

    for doc in mix:
        splits = doc.split()
        label = splits[0]
        feats = splits[1:]

        ret = predict(splits, model, prior)
        if ret['DICT']['POS'] > threshold:
            data.append('\t'.join(['POS'] + feats))
        else:
            data.append('\t'.join(['NEG'] + feats))

    model.clear()
    prior.clear()
    build_nb_model(set(data), model, prior)


def write_model_file(file_name, model, prior):
    out = codecs.open(file_name, mode = 'w', encoding = 'utf-8', errors = 'strict', buffering = 1)
    json.dump([ model, prior ], out)
    out.close()


def main(argv):
    if len(argv) != 4 or argv[1] == '-h':
        print('Usage: sem_learn.py <pos-feature-file> <mix-feature-file> <output-model-file>')
        return
    pos = read_file(argv[1])
    mix = read_file(argv[2])
    print("pos: " + str(len(pos)))
    print("mix: " + str(len(mix)))

    model = {}
    prior = {}

    s_em(model, prior, pos, mix, 10, 5, 1)

    write_model_file(argv[3], model, prior)


if __name__ == "__main__":
    main(sys.argv)
