#!/usr/bin/env python
import sys
import codecs
import io
import nltk
import nltk.data
from nltk.tokenize import word_tokenize


def sentences(text):
    tokenizer = nltk.data.load('tokenizers/punkt/english.pickle')
    sentences = []
    try:
        sentences = tokenizer.tokenize(text)
    except UnicodeDecodeError:
        print >> sys.stderr, "Unicode Error"

    return sentences


def pos_tag(sentence):
    ret = []
    words = nltk.chunk.tree2conlltags(nltk.ne_chunk(nltk.pos_tag(nltk.word_tokenize(sentence))))
    for pair in words: 
        ret.append('/'.join(pair))
    return ret


def get_feats(sentence, label = 'NEG'):
    postagged = pos_tag(sentence)
    return [ label ] + postagged


def extract(item, skip_summary = False):
    ret = []
    url = item[0]
    summary = item[1]
    article = item[2]

    if not skip_summary:
        for sentence in sentences(summary):
            ret.append(get_feats(sentence, label = 'POS'))

    for sentence in sentences(article):
        ret.append(get_feats(sentence, label = 'NEG'))

    return ret


def extract_all(lis, skip_summary = False):
    ret = []
    for item in lis:
        try:
            ret += extract(item, skip_summary)
        except:
            print >> sys.stderr, "Error: " + str(item)
    return ret


def read_file(filename):
    ret = []
    if filename == '<stdin>':
        f = sys.stdin
    else:
        f = codecs.open(filename, mode = 'r', encoding = 'utf-8', errors = 'strict', buffering = 1)
    for line in f:
        ret.append(line.split('\t'))
    f.close()
    return ret


def write_file(filename, lis):
    if filename == '<stdout>':
        f = sys.stdout
    else:
        f = codecs.open(filename, mode = 'w', encoding = 'utf-8', errors = 'strict', buffering = 1)
    for item in lis:
        print >> f, '\t'.join(item)
    f.close()


def main(argv):
    if argv[1] == '-h':
        print 'Usage: feature_extractor.py <data-file> <output-pos-feature-file> <output-mix-feature-file>'
        print '       feature_extractor.py -c <data-file> <output-feature-file>  For classification'
        return
    if argv[1] == '-c':
        ret = extract_all(read_file(argv[2]), skip_summary = True)
        write_file(argv[3], ret)
    else:
        ret = extract_all(read_file(argv[1]))
        write_file(argv[2], [ x for x in ret if x[0] == 'POS' ] )
        write_file(argv[3], [ x for x in ret if x[0] == 'NEG' ] )


if __name__ == '__main__':
    main(sys.argv)
