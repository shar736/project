#!/usr/bin/env python
import sys
import codecs


def main(args):
    if len(args) != 2 or args[1] == '-h':
        print 'Usage: bigrams.py <unigram-feature-file>'
        return
    f = codecs.open(args[1], mode = 'r', encoding = 'utf-8', errors = 'strict', buffering = 1)
    for line in f:
        try:
            splits = line.split('\t')
            res = []
            res.append(splits[0])
            for idx in range(1, len(splits) - 1):
                s1 = splits[idx].split('/')
                s2 = splits[idx + 1].split('/')
                res.append(s1[1] + '/' + s2[2].strip() + '#' + s2[1] + '/' + s2[2])
            print '\t'.join(res).strip()
        except:
            pass

    f.close()


if __name__ == "__main__":
    main(sys.argv)
