Story Extractor
Script : extract_stories.py
This file uses beautiful soup library to extract text body of articles from the url fetched. It takes
all *.nb files in current directory and reads the list of URLs in them and outputs 
the list of summary and associated article.
python extract_stories.py > a.out 

URL Collector
Script : url_extract_NY.sh
It collects URL and summaries from NYTimes archive based on topics given in
script. It uses NYTimes_SummaryandURL.pypython file.



