# -*- coding: utf-8 -*-
"""
Created on Wed Apr 22 11:27:59 2015

@author: tushar
"""



import sys
import glob 
import os

def main():
    
    idirv = sys.argv[1]
    ofile = sys.argv[2]
   # attachtag = sys.argv[3]
    
    outputfile = open(ofile, 'w')
    
    path = '/home/tushar/Downloads/nlp/data_extract_NY/' + idirv + '/*.out'
    
    files= sorted( glob.glob(path) )  
    
    for file in files:     
        tempfile = open(file, 'r')  
        print(os.path.basename(tempfile.name))
        lines = tempfile.readlines()   
        mystr = "\n".join([line.strip() for line in lines])
        outputfile.write(mystr)
        tempfile.close()
        
    outputfile.close()

    
main()  