# -*- coding: utf-8 -*-
"""
Created on Wed Apr 22 11:48:09 2015

@author: tushar
"""
from nytimesarticle import articleAPI
import sys
import json
filed = open((sys.argv[2]+'.nb'), 'w')

sum_url_dict = {}
megalist = []



def parse_articles(articles):
    '''
    This function takes in a response to the NYT api and parses
    the articles into a list of dictionaries
    '''
#    news = []
    for i in articles['response']['docs']:
        dic = {}
        dic['id'] = i['_id']
#        if i['abstract'] is not None:
#            dic['abstract'] = i['abstract'].encode("utf8")
           # print(i['abstract'].encode("utf8"))
        if i['abstract'] is not None:
            dic['headline'] = i['headline']['main'].encode("utf8") + i['abstract'].encode("utf8")
        else:
            dic['headline'] = i['headline']['main'].encode("utf8")
#        if i['snippet'] is not None:
#            dic['headline'] = i['snippet'].encode("utf8")
        word_list = dic['headline'].strip().split()
        if (len(word_list) >= 10):
            sum_url_dict['summary'] = (' '.join(word_list) + '\n')
            sum_url_dict['url'] = i['web_url']
	    json.dump(sum_url_dict, filed)
	    megalist.append(sum_url_dict)
	    print(sum_url_dict['summary'])
            print(sum_url_dict['url'])
#        dic['desk'] = i['news_desk']
#        dic['date'] = i['pub_date'][0:10] # cutting time of day.
#        dic['section'] = i['section_name']
#        if i['snippet'] is not None:
#            dic['snippet'] = i['snippet'].encode("utf8")
#        dic['source'] = i['source']
#        dic['type'] = i['type_of_material']
#        
#        dic['word_count'] = i['word_count']
#        # locations
#        locations = []
#        for x in range(0,len(i['keywords'])):
#            if 'glocations' in i['keywords'][x]['name']:
#                locations.append(i['keywords'][x]['value'])
#        dic['locations'] = locations
#        # subject
#        subjects = []
#        for x in range(0,len(i['keywords'])):
#            if 'subject' in i['keywords'][x]['name']:
#                subjects.append(i['keywords'][x]['value'])
#        dic['subjects'] = subjects   
#        news.append(dic)
#    return(news) 
        
api = articleAPI('cd49057b7e3dcbd30719c8b11f7133ec:17:71924088')


        
def get_articles(date,query):
    '''
    This function accepts a year in string format (e.g.'1980')
    and a query (e.g.'Amnesty International') and it will 
    return a list of parsed articles (in dictionaries)
    for that year.
    '''
    #all_articles= []
    for i in range(1,20): #NYT limits pager to first 100 pages. But rarely will you find over 100 pages of results anyway.
        articles = []        
        articles = api.search(q = query,begin_date = str(date) + '0101', end_date = str(date) + '1231',\
        sort='oldest',\
        page = str(i))
        parse_articles(articles)
        #all_articles = all_articles + articles
    #return(all_articles)

#articles = []

for x in range(int(sys.argv[1]), int(sys.argv[1])+1):
    get_articles(x, sys.argv[2])
    

json.dump(sum_url_dict, filed) 
filed.close()

#print(len(articles))
     
#parse_articles(articles)
     
