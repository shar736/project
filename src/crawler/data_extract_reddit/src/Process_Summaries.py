# -*- coding: utf-8 -*-
"""
Created on Tue Apr 21 23:24:37 2015

@author: tushar
"""

import sys

gen_threshhold = int(sys.argv[2])

summaryfile = open(sys.argv[1], 'r')
newsumaryfile = open('./data/Processed_summaries.txt','w')

for lines in summaryfile:
    word_list = lines.strip().split()
    if (len(word_list) < gen_threshhold):
        continue
    else:
        newsumaryfile.write(" ".join(word for word in word_list) + '\n')
        
summaryfile.close()
newsumaryfile.close()