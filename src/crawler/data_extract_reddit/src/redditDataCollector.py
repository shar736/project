import praw, sys, io
import json

user_agent = ("tushar_agar")
r = praw.Reddit(user_agent = user_agent)
#'worldnews', 'Documentaries', 'Politics', 'Brazil', 'Chile', 'Pakistan', 'Indianews',\
#'UpliftingNews', 'Afghanistan', 'technology', 'TrueNews', 'indepthstories', 'entertainment',
#'NewsOfTheWeird', 'Economics', 'foodforthought', 'Futurology', 'news', 'Tennis',
#'Baseball','Cricket',\
#'Soccer', 'Sports', 'olympics', 'MusicNews', 'DepthHub', 'freethought', 'Iran', 'Iraq', 
subreddit_list = [ 'Israel',\
'Africa', 'USAnews', 'globalhealth', 'Business']

summary_file = open("Summaries.txt", 'w')
url_lsit= []
dict_id = {}

for reddits in subreddit_list:
    subreddit = r.get_subreddit(reddits)
    
    summary_file.write('***************'+reddits+'\n')

    submissions = subreddit.get_top_from_hour(limit = None)
    for submission in submissions:
        if submission.id not in dict_id:
            dict_id[submission.id] = '1'
        else:
            continue
        try:
            print(submission.title)
            summary_file.write(submission.title+'\n')
            url_lsit.append({'url':submission.url,'summary':submission.title})
        except:
            x = 1    
    
    submissions = subreddit.get_rising(limit = None)
    for submission in submissions:
        if submission.id not in dict_id:
            dict_id[submission.id] = '1'
        else:
            continue
        
        try:
            print(submission.title)
            summary_file.write(submission.title+'\n')
            url_lsit.append({'url':submission.url,'summary':submission.title})
        except:
            x = 1
                
    
    submissions = subreddit.get_hot(limit = None)
    for submission in submissions:
        if submission.id not in dict_id:
            dict_id[submission.id] = '1'
        else:
            continue
        
        try:
            print(submission.title)
            summary_file.write(submission.title+'\n')
            url_lsit.append({'url':submission.url,'summary':submission.title})
        except:
            x = 1
            
    
    submissions = subreddit.get_top_from_all(limit = None)
    for submission in submissions:
        if submission.id not in dict_id:
            dict_id[submission.id] = '1'
        else:
            continue
        
        try:
            print(submission.title)
            summary_file.write(submission.title+'\n')
            url_lsit.append({'url':submission.url,'summary':submission.title})
        except:
            x = 1

            
data_file = open('full_listing.nb', 'w')
json.dump(url_lsit, data_file)
data_file.close()
summary_file.close()
