#!/bin/bash
root=/ddrive/MS/Spring15/NLP/project
src=/ddrive/MS/Spring15/NLP/project/src/crawler/nytimes
data=/ddrive/MS/Spring15/NLP/project/data

python3 $src/nytimes.py >> $data/nytimes.txt
sort -u $data/nytimes.txt > $data/temp
mv $data/temp $data/nytimes.txt
wc -l $data/nytimes.txt
date
