import json
import os
from bs4 import BeautifulSoup
import urllib.request
from lxml import html
import sys

for filename in os.listdir('.'):
    if filename.endswith('.nb'):
        f = open(filename)
        lis = json.load(f)
        count = 0
        for item in lis:
            try:
                feats = []
                feats.append(item['url'])
                feats.append(item['summary'].strip())

                page = urllib.request.build_opener(urllib.request.HTTPCookieProcessor).open(item['url']).read()
                parsed_html = BeautifulSoup(page)

                content = []
                for part in parsed_html.body.findAll('p', attrs={'itemprop':'articleBody'}):
                    content.append(part.text)

                feats.append(' '.join(content))

                print('\t'.join(feats))

            except:
                count += 1

        print("Num Errors: " + str(count), file = sys.stderr)

        f.close()
