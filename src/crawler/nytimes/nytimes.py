import sys
import feedparser
from bs4 import BeautifulSoup
import urllib.request
from lxml import html

def main(args):
    url = 'http://www.nytimes.com/services/xml/rss/nyt/GlobalHome.xml'
    feed = feedparser.parse(url)

    for item in feed['items']:
        feats = []
        #feats.append(item['title'])
        #feats.append(item['link'])

        feats.append(item['guid'])
        feats.append(item['summary'].split('<')[0])

        page = urllib.request.build_opener(urllib.request.HTTPCookieProcessor).open(item['link']).read()
        parsed_html = BeautifulSoup(page)

        content = []
        for part in parsed_html.body.findAll('p', attrs={'itemprop':'articleBody'}):
            content.append(part.text)

        feats.append(' '.join(content))

        print('\t'.join(feats))

if __name__ == '__main__':
    main(sys.argv)
